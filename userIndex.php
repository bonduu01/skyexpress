﻿<?php
session_start();
?>
<?php
    $username = $_SESSION["username"];    
    if ($_SESSION["username"] == null) {
        	header("location: errorpwd.htm");
			exit();
    }
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en">
<head>
    <title>Create User Information</title>
    <meta charset="utf-8">

		<link rel="stylesheet" href="../css/reset.css" type="text/css" media="all"/>
		<link rel="stylesheet" href="../css/layout.css" type="text/css" media="all"/>
		<link rel="stylesheet" href="../css/style.css" type="text/css" media="all"/>
        <link rel="stylesheet" type="text/css" href="tcal.css" />
        <script type="text/javascript" src="tcal.js"></script>
		<script type="text/javascript" src="../js/jquery-1.6.js" ></script>
		<script type="text/javascript" src="../js/cufon-yui.js"></script>
		<script type="text/javascript" src="../js/cufon-replace.js"></script>  
		<script type="text/javascript" src="../js/Vegur_300.font.js"></script>
		<script type="text/javascript" src="../js/PT_Sans_700.font.js"></script>
		<script type="text/javascript" src="../js/PT_Sans_5F400.font.js"></script>
		<script type="text/javascript" src="../js/atooltip.jquery.js"></script>

		<!--[if lt IE 9]>
		<script type="text/javascript" src="js/html5.js"></script>
		<link rel="stylesheet" href="css/ie.css" type="text/css" media="all">
		<![endif]-->
		<style type="text/css">
            .style9
            {
                font-size: medium;
            }
            .style10
            {
                width: 161px;
            }
        </style>
		<!--[if lt IE 7]>
		<div style=' clear: both; text-align:center; position: relative;'>
			<a href="../windows.microsoft.com/en-US/internet-explorer/products/ie/home@ocid=ie6_countdown_bannercode">
			<img src="../storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" 	alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." /></a>
			</div>
		<![endif]-->

</head>
<body id="page5">
<div class="main">
<header>
<h2><a href="#">Administration</a></h2>
    <h3>Welcome Back! User: <?php echo $username; ?></h3>
<div>
 <table width="100%" style="margin-left: 0px">
        <tr>
        <td><h4><a href="userRec.php">>>>click to view User record>>> </a></h4></td>
        <td><h4><a href="../default.htm">Home</a></h4></td>
		<!--<td align = "right"><h4><a href="contacthome.php">>>>click to view contact form info>>> </a></h4></td>-->
        </tr>
        </table>               
</div>
</header>
<div class="wrapper">
<form action="uploadUser.php" method="post" name="mytrack" onsubmit="return validate()">
<p>
<h1>Create User </h1>
<table width="100%" align="center" style="margin-left: 0px">
<tr>
                <td align="left" valign="top" class="style10" id="username"><strong>Username:</strong></td>
                <td width="400" align="left" valign="top"><input name="username" type="text" size="70" value="" /></td>
              </tr>
              <tr>
                <td align="left" valign="top" class="style10" id="Td1"><strong>Password:</strong></td>
                <td width="400" align="left" valign="top"><input name="password" type="text" id="password" size="70" value="" /></td>
              </tr>
  
              
</table>
</p>
<p align = "center">
              <input type="submit" name="submit" value="Send Now" style="width: 167px; " class="button1"/>
                      </p>
                      <br /><br />
                      </form>
</div>
</div>
<!--Start of Tawk.to Script--><script type="text/javascript"> var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date(); (function(){ var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];s1.async=true; s1.src='https://embed.tawk.to/5f4235381e7ade5df4432b72/default'; s1.charset='UTF-8'; s1.setAttribute('crossorigin','*'); s0.parentNode.insertBefore(s1,s0); })(); </script> <!--End of Tawk.to Script--> </body>
</html>