-- phpMyAdmin SQL Dump
-- version 3.3.1
-- http://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Generation Time: Nov 19, 2018 at 06:42 AM
-- Server version: 5.1.66
-- PHP Version: 5.2.13

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


--
-- Database: `speeyqdm_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `tracking__`
--

CREATE TABLE IF NOT EXISTS `tracking__` (
  `id` int(100) NOT NULL AUTO_INCREMENT COMMENT 'Primary key',
  `track_number` varchar(200) NOT NULL,
  `ship_date` varchar(100) DEFAULT NULL,
  `delivery_date` varchar(100) DEFAULT NULL,
  `ship_fname` varchar(100) DEFAULT NULL,
  `ship_flocation` varchar(100) DEFAULT NULL,
  `ship_tname` varchar(100) DEFAULT NULL,
  `ship_tlocation` varchar(100) DEFAULT NULL,
  `weight` varchar(100) DEFAULT 'Unspecified',
  `package` varchar(200) DEFAULT NULL,
  `status` varchar(50) DEFAULT NULL,
  `service` varchar(100) DEFAULT NULL,
  `progress1` varchar(100) DEFAULT NULL,
  `date1` varchar(100) DEFAULT NULL,
  `progress2` varchar(100) DEFAULT NULL,
  `date2` varchar(100) DEFAULT NULL,
  `progress3` varchar(100) DEFAULT NULL,
  `date3` varchar(30) DEFAULT NULL,
  `progress4` varchar(100) DEFAULT NULL,
  `date4` varchar(30) DEFAULT NULL,
  `progress5` varchar(100) DEFAULT NULL,
  `date5` varchar(25) DEFAULT NULL,
  `progress6` varchar(100) DEFAULT NULL,
  `date6` varchar(25) DEFAULT NULL,
  `progress7` varchar(100) DEFAULT NULL,
  `date7` varchar(25) DEFAULT NULL,
  `progress8` varchar(100) DEFAULT NULL,
  `date8` varchar(25) DEFAULT NULL,
  `progress9` varchar(100) DEFAULT NULL,
  `date9` varchar(25) DEFAULT NULL,
  `progress10` varchar(100) DEFAULT NULL,
  `date10` varchar(25) DEFAULT NULL,
  `user` varchar(50) DEFAULT 'admin',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=78 ;

--
-- Dumping data for table `tracking__`
--

INSERT INTO `tracking__` (`id`, `track_number`, `ship_date`, `delivery_date`, `ship_fname`, `ship_flocation`, `ship_tname`, `ship_tlocation`, `weight`, `package`, `status`, `service`, `progress1`, `date1`, `progress2`, `date2`, `progress3`, `date3`, `progress4`, `date4`, `progress5`, `date5`, `progress6`, `date6`, `progress7`, `date7`, `progress8`, `date8`, `progress9`, `date9`, `progress10`, `date10`, `user`) VALUES
(27, 'E73460241HIE', '09/05/2018', '16/07/2018', 'Vin Diesel ', 'Istanbul, Turkey ', 'Cherly Varga ', '2619 Lime wood drive Holiday, FL, 34690', ' 9.2 lb', 'Sealed secured box ', 'Your package is currently on hold please contact t', 'First class delivery  ', 'Istanbul (IST), Turkey', '05/07/2018', 'Esenyurt, Turkey', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'phresh'),
(33, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(34, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(44, '12345678', '1233', '123', 'Jayden Lyon ', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'Phresh'),
(45, 'HJ15790268OU', '07/23/2018', '08/03/2018', 'Donnie Wahlberg', 'ebusuud street 18 sirkeci, eminonu fatih, istanbul 34210, turkey', 'Heidi A Boal ', '2320, Aspen Dr woodstock, Ill', '25.05 pounds.', 'Two Sealed Packaged Box', 'Package currently in process to the destination ', 'First class shipping ', 'Currently in Nakhichevan, Turkey ', '07/23/2018', 'Package currently in Athens, Greece ', '07/24/2018', 'Package currently in Rome, Italy ', '07/27/2018', 'Package currently in Madrid, Spain', '07/31/2018', 'Package currently in Mexico city, Mexico ', '08/06/2018', '', '', '', '', '', '', '', '', '', '', 'phresh'),
(49, 'HJ15790268OY', '08/06/2018', '08/13/2018', 'Dwayne Johnson ', 'Maimi, Florida ', 'Sheridan Neville ', '302 Red oak drive Belgrade, Montana 56714', '20Kg ', 'Sealed Packaged Box', 'Package is currently on hold by custom', 'First class shipping ', 'Currently in Miami, Florida ', '08/06/2018', 'Currently in West Palm Beach', 'Currently in Orlando, Florida ', 'Currently In Kansas City, Missouri', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'phresh'),
(52, 'HADR044KS4', '08/09/2018', '08/13/2018', 'Mr. Gleb James Vladimir', 'Erbil Kurdistan, Iraq', 'Miss. Drahomira Sucha', 'Kastanova 1159 Susice 34201, Czech Republic', '15.5 kg', 'Box', 'Parcel Submitted', 'Rapid Logistics express saver (2-4 Dny)', 'Tracking details downloaded from secure server >>>', '08/08/2018', 'Parcel departed > Baghdad, Iraq >>>  Transit Destination > Istanbul, Turkey >>>', '08/09/2018', 'Parcel arrived Turkey > Istanbul Ataturk Int Airport >>>', '08/09/2018', 'Parcel departed Istanbul, Turkey >>> Transit Destination > Kiev, Ukraine >>>', '08/10/2018', 'Parcel arrived Kiev, Ukraine >>> Parcel has been put on Hold by the Kiev custom officials >>>', '08/10/2018', 'Contact shipping company immediately for further clarifications >>>', '08/10/2018', '', '', '', '', '', '', '', '', 'Admin'),
(53, 'AER760943209', '08/10/2018', '08/15/2018', 'Peter Donald', 'Istanbul, Turkey', 'lee Ann Cain ', '609 S E 3rd Newton Ks 67114', '20kg', 'Sealed box', 'Your Package is currently held by customs', 'First class shipping', 'Currently in Istanbul, turkey', '', 'Currently in Athens, Greece ', '08/12/2018', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'phresh'),
(57, 'H568834965K', '08/17/2018', '08/30/2018', 'Thomas Anders', 'Istanbul, Turkey ', 'Linda Deng', '38 Pale Moon Cres , Scarborough , ONM1W 3H5 Canada ', '20 Kg', 'Sealed box ', 'Package is currently on it way to the destination ', 'First class shipping ', ' Istanbul, Turkey ', '08/17/2018', 'Currently in Athens, Greece ', '08/18/2018', 'Currently in Lisbon, Portugal ', '08/22/2018', 'Currently in Madrid, Spain ', '08/22/2018', 'Currently in Kuala lumpur, Malaysia', '08/23/2018', '', '', '', '', '', '', '', '', '', '', 'phresh'),
(58, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(59, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(62, 'EH58394653IJ', '08/20/2018', '08/31/2018', 'Dwayne Johnson', 'Istanbul, Turkey ', 'Antonia O. Martinez', '7314 Midcrown  San Antonio , Texas 78218', '20kg', 'Sealed Box ', 'Package is currently on hold', 'First class delivery  ', 'YeÅŸilkÃ¶y, 34149 BakÄ±rkÃ¶y/Istanbul, Turkey', '08/20/2018', 'Attiki Odos, Spata Artemida 190 04, Greece', '08/21/2018 ', '64000 Sepang, Selangor, Malaysia', '08/22/2018', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'PHRESH'),
(64, '1ZXY22345X6', '08/22/2018', '08/24/2018', 'Michael Cervera', 'RAQQA,SYRIA', 'Jeannie Alsup', 'Louisiana, U.S.A', '27 KG', 'BOX', 'In Transit', 'GLOBAL EXPRESS SAVER (2-3 Days)', 'Tracking details downloaded from secure server >>>', '08/22/2018', 'Parcel departed Syria > Transit Destination > Iran >>>', '08/22/2018', 'Parcel arrived Iran >Tehran Imam Khomeini International Airport, Iran >>>', '08/23/2018', 'Parcel departed Iran > Transit Destination > Brussels, Belgium >>>', '08/23/2018', 'Parcel arrived Brussels, Belgium >>>', '08/24/2018', 'Parcel ON HOLD at Brussels Airport by Customs > Contact shipping company immediately >>>', '08/24/2018', '', '', '', '', '', '', '', '', 'Admin'),
(65, 'HY670943OL', '08/23/2018', '08/27/2018', 'Pablo schreiber', 'Istanbul, Turkey', 'Ann Carlson', '4037 N CAMPBELL CHICAGO,IL 60618 Los Angeles USA ', '20Kg', 'Sealed box', 'package is currently on held by customs ', 'First class shipping ', 'YeÅŸilkÃ¶y, 34149 BakÄ±rkÃ¶y/Istanbul, Turkey', '08/23/2018', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'phresh'),
(66, 'HY13859349T', '08/25/2018', '08/30/2018', 'Donnie Wahlberg ', 'Istanbul, Turkey ', 'Laura Susan Roach', '113 pepperwood crescent Kitchener Ontario Canada n2a 2r4', '20 kg ', 'Sealed box ', 'Package currently on it way to the destination ', 'First class shipping ', 'YeÅŸilkÃ¶y, 34149 BakÄ±rkÃ¶y/Istanbul, Turkey', '08/25/2018', 'Attiki Odos, Spata Artemida 190 04, Greece', '08/25/2018', 'Av de la Hispanidad, s/n, 28042 Madrid, Spain', '08/26/2018', 'Alameda das Comunidades Portuguesas, 1700-111 Lisboa, Portugal', '08/27/2018', '4470-558 Maia, Portugal', '08/29/2018', '', '', '', '', '', '', '', '', '', '', 'phresh'),
(67, 'D5E468532467K', '08/25/2018', '08/30/2018', 'Scott Anderson', 'Houston Texas, United state ', 'zhang xueying', '8 Rongjing East Street, Beijing Economic-Technological  Development Area (BDA),Â Beijing, 100176, P.', '20 Kg ', 'Sealed box ', 'Package currently in process to it destination ', 'First class shipping ', '2800 N Terminal Rd, Houston, TX 77032, USA', '08/25/2018', 'Currently in Istanbul turkey ', '08/28/2018', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'phresh'),
(70, 'SKY8808284', '09/09/2018', '09/11/2018', 'Mr. Mark James Vladimir', 'Erbil Kurdistan, Iraq', 'Miss. Konopka Krystyna', 'ul.Pilsudskiego 38 m. 13 22-300 Krasnystaw. Polska', '13.5 kg', 'Box', 'Parcel Submitted', 'Rapid Logistics express saver (2-3 Days)', 'Tracking details downloaded from secure server >>>', '09/07/2018', 'Parcel departed Iraq > Transit Destination > Istanbul Turkey >>>', '09/08/2018', 'Parcel arrived Turkey > Istanbul Ataturk Int Airport >>>', '09/10/2018', 'Parcel departed Istanbul, Turkey >>> Transit Destination > Kiev, Ukraine >>>', '09/10/2018', '', '', '', '', '', '', '', '', '', '', '', '', 'Admin'),
(71, '670152677THG', '10/9/2018', '12/9/2018', 'æˆ´ç»´æ£®å¼—å…°å…‹', '647ä¼Šæ‹‰å…‹ä¸­å¤®åŒ»é™¢', 'ç”°åˆ©å¨œ', 'æµ™æ±Ÿçœå®æ³¢å¸‚é„žå·žåŒºå§œå±±é•‡æ›™å‡èŠ±è‹‘5å¹¢10å·703å®¤', '10.06 Kg', 'å¯†å°ç›’', 'å¤„ç†é¡¹ç›®637', 'å¤´ç­‰èˆ±äº¤ä»˜', 'ä»Žä¼Šæ‹‰å…‹å›½é™…æœºåœºå‡ºå‘', '10/9/2018', 'åœ¨è½¬ç§»åˆ°ä¸‹ä¸€ä¸ªè®¾æ–½', '11/9/2018', 'æŠµè¾¾ä¼Šæ–¯å¦å¸ƒå°”é˜¿å¡”å›¾å°”å…‹æœºåœºï¼ˆåœŸè€³å…¶ï¼‰', '11/9/2018', 'æŠµè¾¾ç´ ä¸‡é‚£æ™®æœºåœºï¼ˆæ›¼è°·ï¼‰', '11/9/2018', 'æµ·å…³æŒæœ‰', '11/9/2018', '', '', '', '', '', '', '', '', '', '', 'truze'),
(72, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(73, '638636328e', '  14/09/2018', '  17/09/2018', '  Leutinant Roland Mason Martinez', '  é˜¿å¯Œæ±—', '  é™ˆå°å»º', '  é™•è¥¿çœå»¶å®‰å¸‚å¯ŒåŽ¿åŽ¿æ”¿åºœå¯¹é¢å¾¡åŠå ‚å…»ç”Ÿé¦†', '  12KG', '  æ¡†', '  ç¦»å¼€é˜¿å¯Œæ±—å›½é™…æœºåœº', '  ç¬¬ä¸€ç±»æœåŠ¡', '  ç¦»å¼€é˜¿å¯Œæ±—å›½é™…æœºåœº', '  14/09/2018', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'Batsignal30$'),
(76, '5190835512EX', '5/11/2018', '9/11/2018', 'mocolov statham', 'Ontario, CA 91761, USA', 'Ningyo yap mocolov', '301,3/F, One pacific centre, 414 Kwun Tong Road, Kowoloon', '10.05 KG', 'Sealed Box ', 'Departed Ontario International Airport', 'First class Delivery ', 'Departed Ontario International Airport', '5/11/2018', 'In transit to next facility ', '6/11/2018', 'Arrived Istanbul AtatÃ¼rk Airport (Turkey)', '6/11/2018', 'Held by Custom ', '8/11/2018', '', '', '', '', '', '', '', '', '', '', '', '', 'truze'),
(77, '74645645463EX', '7/11/2018', '9/11/2018', 'MARCOS LUCAS', '748 East Mezzeh Al Shafee Street', 'Farzaneh Khaleghi', 'Shiraz city-industries Blv-Arian town-8 Kherad Street-Baran apartment', '10.05 KG', 'Sealed Box ', 'Sky Express in possession of the item ', 'First Class Delivery ', 'Sky Express in possession of the item ', '7/11/2018', 'Departed Damascus International Airport', '8/11/2018', 'Arrived Istanbul AtatÃ¼rk Airport (Turkey)', '8/11/2018', 'Held by Custom ', '8/11/2018', '', '', '', '', '', '', '', '', '', '', '', '', 'truze');
